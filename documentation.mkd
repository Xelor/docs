Title: Writing Documentation
CSS: /css/main.css

#{include head}

Documentation
=============

* This will become a table of contents (this text will be scraped).
{:toc}

## Overview

Documentation is in [::docs] and is entirely in markdown with Maruku extensions.

## Guidelines

- _DO_ document Exherbo-specific tools and processes.
- _DO_ document Exherbo developer workflow and practices.
- _DO NOT_ document basic UNIX commands or information.
- _DO NOT_ document steps for package installation, configuration and usage.

## Style

Your documentation must conform to the existing documentation style. A template
has been provided for you in the file `template.mkd.tmpl`.

- Always include the `Title` and `CSS` headers.
- Include the `head` and `foot` templates at the beginning and end of your
  document, respectively.
- Include a copyright notice just before the footer.

### Extensions and post-processing

There's a few changes to the raw markdown that will occur when they are
converted to HTML.

- `[pkg:category/package-name]` will be converted to links to packages in
  [Summer]. (ex. [pkg:sys-apps/paludis])
- `[pkg:repository/repository-name]` will be converted to links to the
  corrisponding repository page in [Summer]. (ex. [pkg:repository/arbor])
- `[::repository]` will be converted to a link to the repository on Git.
  (ex. [::docs])

- Page titles (`Title: `) will have "Exherbo - " appended to them.

## Testing your documentation

Maruku is required to render the markdown into html. Run `make` in the base directory of the
documentation sources and point your web browser at the files.

The CSS is not included in the repository, so there won't be any formatting.

## Submitting patches for documentation and the website

- We use the Creative Commons Attribution Share Alike 3.0 license for all documentation and
  website content. Contributions using other licenses are unlikely to be accepted.

- Patches should be contributed via our [Gitlab](//gitlab.exherbo.org/exherbo). The `docs` repository
  resides [here](//gitlab.exherbo.org/exherbo-misc/docs).

[Summer]: //git.exherbo.org/summer

--
Copyright 2012 William Orr

Copyright 2015 Kylie McClain

#{include CC_3.0_Attribution_ShareAlike}
#{include foot}

<!-- vim: set tw=100 ft=mkd spell spelllang=en sw=4 sts=4 et : -->
