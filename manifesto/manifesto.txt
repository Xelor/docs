.. vim: set spell spelllang=en tw=100 :

Purpose
=======

Exherbo is a distribution designed for people who know what they're doing with Linux. It is inspired
in many places by Gentoo -- in particular, it supports flexible source-based installation with
up-front configuration.

Exherbo is not a Gentoo fork in the conventional sense. Although it shares some code with Gentoo,
and although many concepts are similar, and although many of the people involved were or are Gentoo
developers, most Exherbo code is rewritten from scratch.

Exherbo is not, at the moment, a user-targeted distribution. It supports packages that the people
involved find interesting or useful; it probably does not support your favourite desktop environment
or applications. That kind of thing will come later -- there are plenty of other options for users
who want a distribution that does everything badly rather than a few things well.

In other words, go and use Gentoo or Ubuntu please.

Why the Need?
=============

It's not that we think that Gentoo is bad. It's just that we think we can do something that suits
our needs better. We've tried, without success, to do this using Gentoo. Unfortunately, Gentoo has
serious shortcomings in several areas that stopped this from being a viable long-term approach.

Aspects we find particularly problematic include:

* Portage. The code is too broken and unmaintainable to deliver the changes we need in the timeframe
  we need. Portage can't deliver even the basic needs of Gentoo; for what we're trying, it's
  completely out of the question. Unfortunately, Gentoo is too tightly attached to Portage at the
  expense of the tree.

* Gentoo management.

* QA. Gentoo developers, by and large, don't consider QA to be their problem. Several prolific
  developers actively flaunt QA, repeatedly break things, refuse to do even simple like keeping a
  proper changelog and generally screw around with the tree. In Gentoo this is considered acceptable
  because spending a little time to do things properly wouldn't be fun (never mind the lack of fun
  for everyone else having to clean up) and because no-one is allowed to tell a volunteer what to
  do. Similarly, maintainers are free to screw up archs out of carelessness, spite or general
  stubbornness.

* The users. Not the real users, but the annoying few dozen vocal users, many of whom haven't
  used Gentoo for years, who go around yelling "Gentoo is about community!" because they don't
  actually use anything Gentoo provides any more. And, combined with that, the disproportionate
  attention that is paid to those users' demands as opposed to real users' needs.

* The developers. Not the ones that do things properly. The ones that either do nothing or
  repeatedly break things.

* Lack of overall design and direction. Gentoo direction is controlled by anyone who can be bothered
  to spend a few weeks working upon some pet project which is then left as a huge mess for everyone
  else to handle. There is also a faulty assumption that because it is a volunteer project,
  developers can't be expected to contribute towards particular project goals.

Ultimately, we believe that Gentoo was a good starting point. However, to provide what we need and
what we want, a new generation of tools and development is required.

Design Goals
============

* Phrase all design goals in such a way that it is hard to use them as slogans to justify stupid
  changes.

* The target user has a good degree of familiarity with Linux.

* No interactivity requirement. Controllable, repeatable behaviour with up-front configuration and
  information.

* Flexibility where it makes sense to provide flexibility.

* No single set of defaults. Rather, a small collection of sets of defaults that are good for their
  target user base, and that between them cover most target user classes.

OK, I Want to Try Exherbo
=========================

No you don't.

Yes I Do
========

OK, maybe you do, but we don't particularly want you to try it because we don't want to deal with
you whining when you find that absolutely nothing works. Exherbo isn't in a fit state for users.  We
might get there one day, but it's not a priority. Right now, all we care about is getting it into a
fit state for a small number of developers.

We don't provide packages for lots of things you consider critical.

A lot of the packages we do provide don't work.

A lot of the packages that worked five minutes ago all just broke because we just decided to
redesign several large features.

We don't provide support.

We don't provide install media.

We don't provide a usable init system.

Really, all we provide is a few things that the few people working on all this find useful for
themselves. When we have something for anyone else, we'll let you know.

But I'm a Developer, and I Want to Try Exherbo
==============================================

Well, you know who to talk to if you need to be told where to find the shiny things.

And no, we don't want to use Exherbo to implement your pet project. Especially not if it's a stupid
pet project. Go and inflict it upon Gentoo, they think that porting ebuilds to run on SunOS 2 ksh
under Cygwin is a great idea.

The above paragraph does not apply if your pet project is something we find interesting.

In Conclusion
=============

It's not that we hate you (unless we do). It's just that we have nothing to offer you, and you have
nothing to offer us.

